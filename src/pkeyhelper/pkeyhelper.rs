#[link(name = "kernel32", kind = "dylib")]
extern "system" {
    fn LoadLibraryA(lpFileName: *const i8) -> *mut c_void;
    fn FreeLibrary(hModule: *mut c_void);
    fn GetProcAddress(hModule: *mut c_void, lpProcName: *const i8) -> *mut c_void;
}

use log::{trace};
use std::{ffi::c_void, mem, ptr};
use super::ipkeyhelper::IPKeyHelper;

pub struct PKeyHelper {
    handle: *mut c_void,
}

impl IPKeyHelper for PKeyHelper {
    fn init() -> Option<Self> {
        unsafe {
            trace!("Allocating internal handle..");
            let handle = LoadLibraryA("pkeyhelper.dll\0".as_ptr() as _);
            if !handle.is_null() {
                Some(Self { handle })
            } else {
                None
            }
        }
    }

    fn get_pk<S: AsRef<str>>(&self, edition_name: S, channel: S) -> Option<String> {
        let edition_id = self.get_edition_id_from_name(edition_name);

        unsafe {
            trace!("Handle base: {:x}", self.handle as usize);

            #[allow(non_snake_case)]
            let SkuGetProductKeyForEdition =
                GetProcAddress(self.handle, "SkuGetProductKeyForEdition\0".as_ptr() as _);
            trace!(
                "Function pointer: {:x}",
                SkuGetProductKeyForEdition as usize
            );

            let raw_channel = {
                let mut a = channel.as_ref().encode_utf16().collect::<Vec<_>>();
                a.push(0);
                a
            };
            trace!("{:?}", raw_channel);

            let mut product_key = std::ptr::null_mut();
            let status = mem::transmute::<_, unsafe extern "C" fn(i32, *const u16, *mut *mut u16, *mut u16) -> i32>(
                SkuGetProductKeyForEdition,
            )(
                edition_id,
                raw_channel.as_ptr() as _,
                &mut product_key as *mut _,
                ptr::null_mut(),
            );
            if status == 0 {
                let mut buf = vec![0u16; 29];
                ptr::copy(product_key, buf.as_mut_ptr(), 29);
                String::from_utf16(&buf).ok()
            } else {
                None
            }
        }
    }

    fn get_edition_id_from_name<S: AsRef<str>>(&self, edition_name: S) -> i32 {
        unsafe {
            trace!("Handle base: {:x}", self.handle as usize);

            #[allow(non_snake_case)]
            let GetEditionIdFromName =
                GetProcAddress(self.handle, "GetEditionIdFromName\0".as_ptr() as _);
            trace!("Function pointer: {:x}", GetEditionIdFromName as usize);

            let raw_edition_name = {
                let mut a = edition_name.as_ref().encode_utf16().collect::<Vec<_>>();
                a.push(0);
                a
            };
            trace!("{:?}", raw_edition_name);

            let mut eid = 0;
            mem::transmute::<_, unsafe extern "C" fn(*const u16, *mut i32)>(GetEditionIdFromName)(
                raw_edition_name.as_ptr() as _,
                &mut eid as _,
            );
            eid
        }
    }
}

impl Drop for PKeyHelper {
    fn drop(&mut self) {
        trace!("Freeing internal handle..");
        unsafe {
            if !self.handle.is_null() {
                FreeLibrary(self.handle)
            }
        }
    }
}
