mod pkeyhelper;
mod ipkeyhelper;

pub use pkeyhelper::PKeyHelper;
pub use ipkeyhelper::IPKeyHelper;

#[test]
fn it_works() {
    struct MPKeyHelper {
        force_edition_id: i32,
        force_pk: String,
    }
    
    impl IPKeyHelper for MPKeyHelper {
        fn init() -> Option<Self> {
            // EnterpriseS Volume
            Some(Self { force_edition_id: 125, force_pk: "M7XTQ-FN8P6-TTKYV-9D4CC-J462D".to_owned()  })
        }
    
        fn get_pk<S: AsRef<str>>(&self, _: S, _: S) -> Option<String> {
            Some(self.force_pk.to_owned())
        }
    
        fn get_edition_id_from_name<S: AsRef<str>>(&self, _: S) -> i32 {
            self.force_edition_id
        }
    }

    let pkeyhelper = PKeyHelper::init().unwrap();
    let mpkeyhelper = MPKeyHelper::init().unwrap();
    
    assert_eq!(pkeyhelper.get_edition_id_from_name("EnterpriseS"), mpkeyhelper.get_edition_id_from_name(""));
    assert_eq!(pkeyhelper.get_pk("EnterpriseS", "Volume"), mpkeyhelper.get_pk("", ""));
    assert_ne!(pkeyhelper.get_pk("EnterpriseS", "OEM"), mpkeyhelper.get_pk("", ""));
}