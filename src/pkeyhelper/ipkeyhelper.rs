pub trait IPKeyHelper: Sized {
    fn init() -> Option<Self>;
    fn get_pk<S: AsRef<str>>(&self, edition_name: S, channel: S) -> Option<String>;
    fn get_edition_id_from_name<S: AsRef<str>>(&self, edition_name: S) -> i32;
}