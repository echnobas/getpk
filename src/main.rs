macro_rules! input {
    ($($arg:tt)*) => {{
        use ::std::io::Write;
        (|| -> ::std::io::Result<String> {
            print!("{}", ::std::fmt::format(::std::format_args!($($arg)*)));
            ::std::io::stdout().flush()?;
            let mut res = ::std::string::String::new();
            ::std::io::stdin().read_line(&mut res)?;
            res.truncate(res.trim().len());
            Ok(res)
        })()
    }}
}

mod pkeyhelper;
use pkeyhelper::PKeyHelper;
use pkeyhelper::IPKeyHelper;

fn main() {
    env_logger::init();
    let pkeyhelper = PKeyHelper::init().unwrap();
    let edition = input!("Edition :: ").unwrap();
    let channel = input!("Channel :: ").unwrap();
    println!("  Key   :: {}", pkeyhelper.get_pk(&edition, &channel).unwrap_or("UNKNOWN".to_owned()));
    loop {
        std::thread::sleep_ms(60_000);
    }
}