$env:RUSTFLAGS = "-Clink-arg=/DEBUG:NONE -Clink-arg=/Brepro -Ctarget-feature=+crt-static --remap-path-prefix=$home=."
$exe="$(Split-Path -Path (Get-Location) -Leaf).exe"
$arch="x86_64"
cargo clean
cargo build --target "$arch-pc-windows-msvc" --release
Set-AuthenticodeSignature ".\target\$arch-pc-windows-msvc\release\$exe" -Certificate (Get-ChildItem Cert:\CurrentUser\My -CodeSigningCert)
copy ".\target\$arch-pc-windows-msvc\release\$exe" .